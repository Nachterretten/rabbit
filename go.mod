module rabbti

go 1.20

require (
	github.com/rabbitmq/amqp091-go v1.8.1
	google.golang.org/protobuf v1.31.0
)
