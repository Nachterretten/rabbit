package main

import (
	amqp "github.com/rabbitmq/amqp091-go"
	"google.golang.org/protobuf/proto"
	"log"
	order_proto "rabbti/docs"
	"time"
)

func main() {
	con, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	if err != nil {
		log.Fatal("Failed to connect to RabbitMQ", err)
	}
	defer con.Close()

	ch, err := con.Channel()
	if err != nil {
		log.Fatal("Failed to open a channel", err)
	}

	q, err := ch.QueueDeclare("order_queue", false, false, false, false, nil)
	if err != nil {
		log.Fatal("Failed to declare a queue", err)
	}

	statusQueue, err := ch.QueueDeclare("status_queue", false, false, false, false, nil)
	if err != nil {
		log.Fatal("Failed to declare a queue", err)
	}

	order := &order_proto.Order{
		OrderId:     1,
		ProductName: "test",
		Quantity:    1,
		TotalPrice:  10,
		Client:      1,
	}

	orderBytes, err := proto.Marshal(order)
	if err != nil {
		log.Fatal("Failed to marshal order", err)
	}

	err = ch.Publish("", q.Name, false, false, amqp.Publishing{
		ContentType: "application/json",
		Body:        orderBytes,
	})
	if err != nil {
		log.Fatal("Failed to publish a message", err)
	}
	log.Println("Message published")

	statusMessages, err := ch.Consume(statusQueue.Name, "", true, false, false, false, nil)
	if err != nil {
		log.Fatal("Failed to register a consumer", err)
	}

	go func() {
		for msg := range statusMessages {
			status := &order_proto.OrderStatus{}
			if err := proto.Unmarshal(msg.Body, status); err != nil {
				log.Printf("Failed to unmarshal message: %s", err)
				continue
			}
			log.Printf("Status: %v", status)
		}
	}()
	time.Sleep(1 * time.Second)
}
