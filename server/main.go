package main

import (
	amqp "github.com/rabbitmq/amqp091-go"
	"google.golang.org/protobuf/proto"
	"log"
	order_proto "rabbti/docs"
)

func main() {
	con, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	if err != nil {
		log.Fatalf("Failed to connect to RabbitMQ: %s", err)
	}
	defer con.Close()

	ch, err := con.Channel()
	if err != nil {
		log.Fatalf("Failed to open a channel: %s", err)
	}
	defer ch.Close()

	q, err := ch.QueueDeclare("order_queue", false, false, false, false, nil)
	if err != nil {
		log.Fatalf("Failed to declare a queue: %s", err)
	}

	statusQueue, err := ch.QueueDeclare("status_queue", false, false, false, false, nil)
	if err != nil {
		log.Fatalf("Failed to declare a queue: %s", err)
	}

	messages, err := ch.Consume(q.Name, "", true, false, false, false, nil)
	if err != nil {
		log.Fatalf("Failed to register a consumer: %s", err)
	}

	forever := make(chan bool)

	go func() {
		for msg := range messages {
			order := &order_proto.Order{}
			if err := proto.Unmarshal(msg.Body, order); err != nil {
				log.Fatalf("Failed to unmarshal a message: %s", err)
				continue
			}
			log.Printf("Received a message: %v", order)
			status := &order_proto.OrderStatus{
				OrderId: order.OrderId,
				Success: true,
				Message: "order received",
			}

			statusBytes, err := proto.Marshal(status)
			if err != nil {
				log.Printf("Failed to marshal a message: %s", err)
				continue
			}
			err = ch.Publish(
				"",
				statusQueue.Name,
				false,
				false,
				amqp.Publishing{
					ContentType: "application/json",
					Body:        statusBytes,
				})
			if err != nil {
				log.Printf("Failed to publish a message: %s", err)
				continue
			}
		}
	}()
	log.Printf(" [*] Waiting for messages. To exit press CTRL+C")
	<-forever
}
